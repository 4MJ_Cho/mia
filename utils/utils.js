const puppeteer = require('puppeteer');

exports.convertFile = async html => {
  try {
  const browser = await puppeteer.launch({
    args: [
        '--no-sandbox',
        '--disable-setuid-sandbox'
    ]
});
  const page = await browser.newPage()
  await page.setContent(html);
  const src = new Date().getTime();
  await page.pdf({ path: `applications/form${src}.pdf`, format: 'A4' });
  await browser.close();
  console.log('Pdf generated...');
  // console.log('page source', `application/form${src}`);
  return `form${src}.pdf`;
  } catch (err) {
    console.error(err, 'Something went wrong');
  }
};