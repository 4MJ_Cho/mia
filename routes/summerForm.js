const express = require("express");
const mailer = require("nodemailer");
const smtp = require("nodemailer-smtp-transport");
const router = express.Router();
const path = require('path');
const fs = require('fs');
// require("dotenv").config();
const { convertFile } = require('../utils/utils');

// Display the summer page
router.get("/", (req, res) => {
  res.render("pages/summer");
});

// Render welcome page
router.get("/welcome", (req, res) => {
  res.render("pages/welcome");
});

// Send the application form to the provided email
router.post("/", (req, res) => {
  const {
    name1,
    name2,
    email,
    address1,
    address2,
    city,
    region,
    code,
    area,
    phone,
    question1,
    question2,
    series,
    question3,
    question4,
    question5,
    question6,
    question7,
    question8,
    question9,
    question10,
    message1,
    message2,
    message3,
    message4,
    message5,
  } = req.body;
  let form;

  form = {
    name1,
    name2,
    email,
    address1,
    address2,
    city,
    region,
    code,
    area,
    phone,
    question1,
    question2,
    series,
    question3,
    question4,
    question5,
    question6,
    question7,
    question8,
    question9,
    question10,
    message1,
    message2,
    message3,
    message4,
    message5,
  };
  console.log(form);
  const msg = {
    to: ['cristina@open-dreams.org', "Tikuakuro25@gmail.com"],
    html: `
        <div class="div">
        <h2>First name: ${name1}</h2>
        <h2>Second name: ${name2}</h2>
        <h2>Email: ${email}</h2>
        <h2>Address line 1: ${address1}</h2>
        <h2>Address line 2: ${address1}</h2>
        <h2>City: ${city}</h2>
        <h2>Region: ${region}</h2>
        <h2>Postal zip/code: ${code}</h2>
        <h2>Area code: ${area}</h2>
        <h2>Phone: ${phone}</h2>
        <h2>Have you ever applied to the summer academy before?: ${question1}</h2>
        <br/>
        <h2>Academic Information and Extracurricular Involvement</h2>
        <br/>
        <h2>What is your current or most recent Secondary/High School: ${question2}</h2>
        <h2>Are You in the Arts, Sciences, or Commercial Area of studies?: ${series}</h2>
        <h2>Have you already taken either both the GCE Ordinary level and Advanced Level? If yes,enter Exams taken, if no, skip the next 4 questions: ${question3}</h2>
        <h2>What Year was/were the above Exam(s) taken?</h2>
        <h2>Ordinary Level: ${question4}</h2>
        <h2>Advanced Level: ${question5}</h2>
        <h2>Enter letter grades and number of points obtained for the subjects passed</h2>
        <h2>Ordinary Level: ${question6}</h2>
        <h2>Advanced Level: ${question7}</h2>
        <h2>Did/Do you belong to any club(s) on campus? ${question8}<h2>
        <h2>Did/Do you hold any leadership role on campus?: ${question9}</h2>
        <h2>Have you ever been involved with any NGO/Community Based Organisation?: ${question10}</h2>
        <p>List up to 3 Organisation you have involved with: ${message1}</p>
        <p>List up to 5 of your most recent and significant extracurricullar involvements and awards: ${message2}</p>
        <h3 style="color: gold; font-size: 18px;"><br><br>Essay Questions<br></h3>
        <p>Tell us about Yourself and your Family: ${message3}</p>
        <p>What are your career aspirations in the long term goals?: ${message4}</p>
        <p>In few words, describe your most significant extracurricular achievements: ${message5}</p>
        </div>
    `,
  };

  async function newmailjet() {
    const transport = mailer.createTransport(
      smtp({
        host: "in.mailjet.com",
        port: 2525,
        auth: {
          user:
            process.env.MAILJET_API_KEY || "50877fd9f0e6d0a3ee9528c48fafabf5",
          pass:
            process.env.MAILJET_API_SECRET ||
            "9e984acf75ef11b05b67878c1754d026",
        },
      })
    );
    try {
      // const src = await convertFile(msg.html);
      const json = await transport.sendMail({
        from: "info@open-dreams.org", // From address
        // to: ['marcsitze01@gmail.com', 'Tikuakuro25@gmail.com'],
        to: ['cristina@open-dreams.org', 'Tikuakuro25@gmail.com', 'james@open-dreams.org'], // To address
				subject: "Summer Application form", // Subject
        html: msg.html, // Content
        // attachments: [{
        //   filename: src,
        //   path: path.join(__dirname, `../applications/${src}`),
        //   contentType: 'application/pdf',
        // }],
      });
      console.log(json);

      // fs.unlink(path.join(__dirname, `../applications/${src}`), err => {
      //   if(err) throw Error();
      // });
    } catch (err) {
      console.error("Something went wrong: ", err);
    }
  }

  newmailjet()
  .then(data =>{
		res.status(201).redirect('/');
  })
  .catch(err => {
      console.log(err);
			res.send('Server Error..');
   });
});
module.exports = router;
